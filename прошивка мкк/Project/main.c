#include "main.h"
#include "stm32f429xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f429i_discovery_lcd.h"
#include "stm32f429i_discovery_ts.h"
#include "stdio.h"
#include "stdint.h"
#include <math.h>
#include "sample_data.h"
#define PI 3.1415926536

ADC_HandleTypeDef hadc1;
DAC_HandleTypeDef hdac;
TIM_HandleTypeDef htim1;

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_DAC_Init(void);
static void MX_TIM1_Init(void);

void effect(int number);
void None_effect(void);
void Delay_effect(void);
void Echo_effect(void);
void Flanger_effect(void);
void Reverb_effect(void);
void Lowpass_effect(void);
void Bandpass_effect(void);
void PitchShift_effect(void);
uint16_t GetTestSound(void);

// variables
int x, y;
int time = 0;
int amplitude;
int N_samples = 1024;
int i_sample = 0;
int sample_finish_flag = 0;
int cur_effect = 0;
int work_mode = 0;
int volume = 100;
int parameter_flag = 0;
float sr = 84000000/(2*10*381);

// arrays with microphone sound data
int input[1024] = {0};
int* INPUT = input;
int data[1024] = {0};
int* DATA = data;
int output[1024] = {0};
int* OUTPUT = output;
int next_output[1024] = {0};
int* NEXT_OUTPUT = next_output;

int queue[22050] = {0};

// array with prerecorded music data
uint16_t test_sound[] = {MUSIC_DATA};
unsigned int i_testsound = 0;

int main()
{
  HAL_Init();
  SystemClock_Config();
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_DAC_Init();
  MX_TIM1_Init();
  
  BSP_LCD_Init();
  BSP_LCD_LayerDefaultInit(LCD_BACKGROUND_LAYER, LCD_FRAME_BUFFER);
  BSP_LCD_SelectLayer(LCD_BACKGROUND_LAYER);
  BSP_LCD_DisplayOn();
  BSP_LCD_Clear(LCD_COLOR_BLACK);
  BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
  BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
  BSP_LCD_DisplayOn();

  BSP_LCD_DrawLine(10, 160, 30, 120);
  BSP_LCD_DrawLine(30, 120, 30, 200);
  BSP_LCD_DrawLine(30, 200, 10, 160);
  
  BSP_LCD_DrawLine(230, 160, 210, 120);
  BSP_LCD_DrawLine(210, 120, 210, 200);
  BSP_LCD_DrawLine(210, 200, 230, 160);
  
  BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
  BSP_LCD_DrawEllipse(18, 20, 7, 10);
  BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
  BSP_LCD_FillRect(0, 0, 30, 25);
  BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
  BSP_LCD_FillEllipse(18, 17, 5, 9);
  BSP_LCD_DrawLine(13, 35, 23, 35);
  BSP_LCD_DrawLine(18, 30, 18, 35);
  
  BSP_LCD_FillRect(30, 290, 180, 2);
  int pos = (int)(volume*1.8 + 30);
  BSP_LCD_FillCircle(pos, 290, 5);
  BSP_LCD_SetFont(&Font16);
  uint8_t str[12];
  sprintf((char*)str, "Volume: %d", volume);
  BSP_LCD_DisplayStringAt(0, 300, str, CENTER_MODE);
  BSP_LCD_SetFont(&Font24);
 
  BSP_TS_Init(240, 320);
  BSP_TS_ITConfig();
  
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
  HAL_ADC_Start_IT(&hadc1);
  
  while(1)
  {
    effect(cur_effect);
  }
  return 0;
}

uint16_t GetTestSound()
{
  uint16_t val = 0;
  if(i_testsound < sizeof(test_sound)/2) val = test_sound[i_testsound];
  i_testsound += 1;
  if(i_testsound == sizeof(test_sound)/2) i_testsound = 0;
  return val;
}

void effect(int number)
{
  switch(number)
    {
    case 0:
      None_effect();
      break;
    case 1: 
      Delay_effect();
      break;
    case 2:
      Echo_effect();
      break;
    case 3:
      Flanger_effect();
      break;
    case 4:
      Reverb_effect();
      break;
    case 5:
      Lowpass_effect();
      break;
    case 6:
      Bandpass_effect();
      break;
    case 7:
      PitchShift_effect();
      break;
    }
}

void None_effect(void)
{
  BSP_LCD_DisplayStringAt(0, 150, "None", CENTER_MODE);
  int i = -1;
  while(cur_effect == 0)
  {
    if (i != i_sample)
    {
      i = i_sample;
      *(OUTPUT + i) = amplitude;
    }
    if(parameter_flag == 1)
    {
      parameter_flag = 0;
    }
  }
  BSP_LCD_DisplayStringAt(0, 150, "         ", CENTER_MODE);
}

void Delay_effect(void)
{
  BSP_LCD_DisplayStringAt(0, 150, "Delay", CENTER_MODE);
  
  float delay_time = 1.;
  float max_delay_time = 2.;
  BSP_LCD_FillRect(30, 240, 180, 2);
  int pos = (int)(delay_time*180./max_delay_time + 30);
  BSP_LCD_FillCircle(pos, 240, 5);
  BSP_LCD_SetFont(&Font16);
  uint8_t str[13];
  sprintf((char*)str, "Delay: %.2f s", delay_time);
  BSP_LCD_DisplayStringAt(0, 250, str, CENTER_MODE);
  BSP_LCD_SetFont(&Font24);
        
  int arr_length = (int)(delay_time*sr);
  int i_start = 0;
  int i = -1;
  while(cur_effect == 1)
  {
    if(i != i_sample)
    {
      i = i_sample;
      queue[i_start] = amplitude;
      i_start += 1;
      if(i_start >= arr_length) i_start = 0;
      *(OUTPUT + i) = queue[i_start];
    }
    if(parameter_flag == 1)
    {
      parameter_flag = 0;
      if(y > 230 && y < 250 && x < 220 && x > 20)
      {
        if (x < 30) x = 30;
        if (x > 210) x = 210;
        delay_time = (x - 30)*max_delay_time/180.;
        arr_length = (int)(delay_time*sr);
        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
        BSP_LCD_FillRect(24, 235, 193, 11);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_FillRect(30, 240, 180, 2);
        pos = (int)(delay_time*180/max_delay_time + 30);
        BSP_LCD_FillCircle(pos, 240, 5);
        BSP_LCD_SetFont(&Font16);
        sprintf((char*)str, "Delay: %.2f s", delay_time);
        BSP_LCD_DisplayStringAt(0, 250, str, CENTER_MODE);
        BSP_LCD_SetFont(&Font24);
      }
    }
  }
  BSP_LCD_DisplayStringAt(0, 150, "         ", CENTER_MODE);
  BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
  BSP_LCD_FillRect(24, 235, 193, 30);
  BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
}

void Echo_effect(void)
{
  BSP_LCD_DisplayStringAt(0, 150, "Echo", CENTER_MODE);
  
  float delay_time = 0.1;
  float max_delay_time = 0.65;
  BSP_LCD_FillRect(30, 240, 180, 2);
  int pos = (int)(delay_time*180/max_delay_time + 30);
  BSP_LCD_FillCircle(pos, 240, 5);
  BSP_LCD_SetFont(&Font16);
  uint8_t str[13];
  sprintf((char*)str, "Delay: %.2f s", delay_time);
  BSP_LCD_DisplayStringAt(0, 250, str, CENTER_MODE);
  BSP_LCD_SetFont(&Font24);
  
  int arr_length = (int)(3.*delay_time*sr);
  int dt = (int)(delay_time*sr);
  int i_start = 0;
  int i = -1;
  while(cur_effect == 2)
  {
    if(i != i_sample)
    {
      i = i_sample;
      queue[(i_start + dt + 1)%arr_length] += amplitude/2;
      queue[(i_start + 2*dt + 1)%arr_length] += amplitude/3;
      queue[(i_start + 3*dt + 1)%arr_length] += amplitude/6;
      i_start += 1;
      if(i_start >= arr_length) i_start = 0;
      *(OUTPUT + i_sample) = amplitude/2 + queue[i_start]/2;
      queue[i_start] = 0;
    }
    if(parameter_flag == 1)
    {
      parameter_flag = 0;
      if(y > 230 && y < 250 && x < 220 && x > 20)
      {
        if (x < 30) x = 30;
        if (x > 210) x = 210;
        delay_time = (x - 30)*max_delay_time/180.;
        arr_length = (int)(3.*delay_time*sr);
        dt = (int)(delay_time*sr);
        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
        BSP_LCD_FillRect(24, 235, 193, 11);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_FillRect(30, 240, 180, 2);
        pos = (int)(delay_time*180/max_delay_time + 30);
        BSP_LCD_FillCircle(pos, 240, 5);
        BSP_LCD_SetFont(&Font16);
        sprintf((char*)str, "Delay: %.2f s", delay_time);
        BSP_LCD_DisplayStringAt(0, 250, str, CENTER_MODE);
        BSP_LCD_SetFont(&Font24);
      }
  }
  }
  BSP_LCD_DisplayStringAt(0, 150, "         ", CENTER_MODE);
  BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
  BSP_LCD_FillRect(24, 235, 193, 30);
  BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
}

void Flanger_effect()
{
  BSP_LCD_DisplayStringAt(0, 150, "Flanger", CENTER_MODE);
  
  float delay_time = 0.015;
  float max_delay_time = 0.05;
  BSP_LCD_FillRect(30, 240, 180, 2);
  int pos = (int)(delay_time*180/max_delay_time + 30);
  BSP_LCD_FillCircle(pos, 240, 5);
  BSP_LCD_SetFont(&Font16);
  uint8_t str[13];
  sprintf((char*)str, "Delay: %d ms", (int)(1000*delay_time));
  BSP_LCD_DisplayStringAt(0, 250, str, CENTER_MODE);
  BSP_LCD_SetFont(&Font24);
  
  float w = 2.;
  int i = -1;
  int i_start = 0;
  int n = 0;
  int arr_len = (int)ceil(delay_time*sr);
  while(cur_effect == 3)
  {
    if(i != i_sample)
    {
      i = i_sample;
      int dt = (int)fabs(sr*delay_time*sin(2*PI*w*(float)n/sr));
      n += 1;
      if(n >= sr) n = 0;
      queue[(i_start + dt + 1)%arr_len] = amplitude;
      i_start += 1;
      if(i_start >= arr_len) i_start = 0;
      *(OUTPUT + i) = amplitude/2 + queue[i_start]/2;
      queue[i_start] = 0;
    }
    if(parameter_flag == 1)
    {
      parameter_flag = 0;
      if(y > 230 && y < 250 && x < 220 && x > 20)
      {
        if (x < 30) x = 30;
        if (x > 210) x = 210;
        delay_time = (x - 30)*max_delay_time/180.;
        arr_len = (int)ceil(delay_time*sr);
        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
        BSP_LCD_FillRect(24, 235, 193, 11);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_FillRect(30, 240, 180, 2);
        pos = (int)(delay_time*180/max_delay_time + 30);
        BSP_LCD_FillCircle(pos, 240, 5);
        BSP_LCD_SetFont(&Font16);
        sprintf((char*)str, "Delay: %d ms", (int)(1000*delay_time));
        BSP_LCD_DisplayStringAt(0, 250, str, CENTER_MODE);
        BSP_LCD_SetFont(&Font24);
      }
    }
  }
  BSP_LCD_DisplayStringAt(0, 150, "         ", CENTER_MODE);
  BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
  BSP_LCD_FillRect(24, 235, 193, 30);
  BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
}

void Reverb_effect(void)
{
  BSP_LCD_DisplayStringAt(0, 150, "         ", CENTER_MODE);
  BSP_LCD_DisplayStringAt(0, 150, "Reverb", CENTER_MODE);
  float sum;
  float overlap_add[256] = {0};
  float reverb_sample[256] = {-0.02179,-0.04934,0.01666,-0.00991,0.00232,0.06970,-0.00569,-0.01542,-0.04508,-0.00789,0.01290,
 -0.03982,-0.03654,0.06408,0.00445,0.00126,-0.00416,0.01581,0.05989,-0.04796,0.01505,0.02763,-0.01692,0.01444,-0.01332,0.01496,
 -0.02290,0.01332,0.08131,-0.00637,0.02637,-0.08526,-0.02214,0.01453,-0.01168,0.00927,-0.00744,0.00562,-0.00301,-0.00122,0.05228,
 0.00792,0.00133,-0.03447,-0.03614,0.02102,-0.01869,0.02152,-0.08525,0.08823,0.06567,0.06404,-0.00150,-0.03483,0.01043,0.01379,
 0.05641,-0.02446,0.04098,0.04371,0.08243,-0.04896,0.01289,-0.00468,0.00086,0.00444,-0.01204,0.05291,-0.03272,-0.01514,0.00309,
 0.04588,0.01907,-0.05988,-0.00804,0.00851,-0.04347,-0.04416,-0.02596,0.03892,0.03157,0.05151,-0.02511,-0.02212,-0.04934,0.09140,
 0.04859,0.01833,-0.00326,-0.08711,0.08904,-0.02508,0.02196,-0.02431,0.04931,-0.02264,-0.00060,0.10660,0.03209,0.04013,0.05375,
 0.02002,-0.03711,0.02313,-0.05819,-0.01512,0.01029,-0.08016,-0.03192,-0.02872,-0.02797,0.01292,-0.06570,0.00586,-0.00872,0.03685,
 0.02446,-0.02693,-0.04486,0.00713,-0.00398,0.00371,-0.01153,-0.06901,0.07651,-0.11443,0.03724,0.14998,0.00159,0.00059,0.04674,
 0.03521,0.00692,0.07095,0.01914,-0.05537,0.04673,-0.00235,-0.06043,-0.10563,-0.07209,0.11743,-0.13366,-0.06242,0.10571,0.01599,
 0.09014,0.14524,0.05590,0.01263,-0.01797,0.02075,0.05300,0.07979,0.07320,0.02046,0.05713,0.07735,0.22275,0.05963,-0.00946,0.00950,
 -0.01118,0.02207,-0.04234,-0.02784,0.08838,0.09040,-0.00566,0.09104,-0.01618,-0.00519,0.01877,-0.06560,-0.02681,0.05471,0.05062,
 -0.02266,-0.02562,-0.02712,0.01379,-0.01079,0.00811,-0.00988,0.01273,-0.04520,-0.02952,0.01162,-0.00876,0.00458,-0.00188,-0.01331,
 -0.04630,0.02070,-0.01793,0.01705,-0.01899,0.02709,0.02707,0.01079,-0.00069,-0.02970,0.01215,-0.00929,0.08247,0.02214,-0.00616,
 0.00451,0.04370,0.06418,-0.00250,0.00364,0.04739,0.00270,0.06522,-0.00151,0.07250,0.05634,-0.02558,0.04192,-0.02418,0.03362,-0.07222,
 0.03685,0.01968,0.11995,0.07818,0.02658,0.08335,0.01928,0.13751,0.03109,0.04314,0.10219,0.04847,-0.03241,0.01842,-0.01163,-0.00135,
 -0.04695,0.05187,-0.03462,0.01848,0.19293,-0.06180,-0.06307,-0.03571,-0.01778,-0.08379,0.03576,0.03076,-0.04638,-0.00604,0.00621,
 -0.00398,-0.00056};
 
  while(cur_effect == 4)
  {
    if(sample_finish_flag == 1)
    {
      sample_finish_flag = 0;
      for(int i = 0; i < 1024 + 256; i++)
      {
        sum = 0;
        for(int j = 0; j < 256; j++)
        {
          if( i - j >= 0 && i - j < 1024) sum += *(DATA + i - j)*reverb_sample[j];
        }
        if(i < 256) sum += overlap_add[i];
        if(i >= 1024) overlap_add[i - 1024] = sum;
        if(i < 1024) *(NEXT_OUTPUT + i) = (int)sum;
      }
    }
    if(parameter_flag == 1)
    {
      parameter_flag = 0;
    }
  }
  BSP_LCD_DisplayStringAt(0, 150, "         ", CENTER_MODE);
}

// 2nd order Butterworth low pass filter
void Lowpass_effect(void)
{
  BSP_LCD_DisplayStringAt(0, 150, "Lowpass", CENTER_MODE);
  
  float f_cutoff = 300;
  float max_f = 2000;
  BSP_LCD_FillRect(30, 240, 180, 2);
  int pos = (int)(f_cutoff*180./max_f + 30);
  BSP_LCD_FillCircle(pos, 240, 5);
  BSP_LCD_SetFont(&Font16);
  uint8_t str[18];
  sprintf((char*)str, "Freq: %d Hz", (int)f_cutoff);
  BSP_LCD_DisplayStringAt(0, 250, str, CENTER_MODE);
  BSP_LCD_SetFont(&Font24);
  
  float ff = f_cutoff/sr;
  float coef = 1.0/tan(PI*ff);
  float sqrt2 = 1.41421356;
  
  float b0, b1, b2, a1, a2;
  b0 = 1.0 / (1.0 + sqrt2*coef + coef*coef);
  b1 = 2*b0;
  b2 = b0;
  a1 = 2.0 * (coef*coef - 1.0) * b0;
  a2 = -(1.0 - sqrt2*coef + coef*coef) * b0;
  
  int x0, x1, x2;
  int y1 = 0; int y2 = 0;
  int prevx1 = 0; int prevx2 = 0;
  int i = -1;
  
  while(cur_effect == 5)
  {
    if(i != i_sample)
    {
      int temp;
      i = i_sample;
      if(i == 0)
      {
        x1 = prevx1;
        x2 = prevx2;
      }
      else if(i == 1)
      {
        x1 = *(INPUT + i - 1);
        x2 = prevx1;
      }
      else
      {
        x1 = *(INPUT + i - 1);
        x2 = *(INPUT + i - 2);
      }
      if (i == 1023)
      {
        prevx1 = *(INPUT + i);
        prevx2 = *(INPUT + i - 1);
      }
      x0 = *(INPUT + i);
      temp = (int)(b0*x0 + b1*x1 + b2*x2 + a1*y1 + a2*y2);
      *(OUTPUT + i) = temp;
      y2 = y1;
      y1 = temp;
    }
    if(parameter_flag == 1)
    {
      parameter_flag = 0;
      if(y > 230 && y < 250 && x < 220 && x > 20)
      {
        if (x < 30) x = 30;
        if (x > 210) x = 210;
        f_cutoff = (float)(x - 30)*max_f/180.;
        BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
        BSP_LCD_FillRect(24, 235, 193, 11);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_FillRect(30, 240, 180, 2);
        pos = (int)(f_cutoff*180./max_f + 30);
        BSP_LCD_FillCircle(pos, 240, 5);
        BSP_LCD_SetFont(&Font16);
        BSP_LCD_DisplayStringAt(0, 250, "              ", CENTER_MODE);
        sprintf((char*)str, "Freq: %d Hz", (int)f_cutoff);
        BSP_LCD_DisplayStringAt(0, 250, str, CENTER_MODE);
        BSP_LCD_SetFont(&Font24);
        
        ff = f_cutoff/sr;
        coef = 1.0/tan(PI*ff);
        b0 = 1.0 / (1.0 + sqrt2*coef + coef*coef);
        b1 = 2*b0;
        b2 = b0;
        a1 = 2.0 * (coef*coef - 1.0) * b0;
        a2 = -(1.0 - sqrt2*coef + coef*coef) * b0;
      }
    }
  }
  BSP_LCD_DisplayStringAt(0, 150, "         ", CENTER_MODE);
  BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
  BSP_LCD_FillRect(24, 235, 193, 30);
  BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
}

void Bandpass_effect(void)
{
  BSP_LCD_DisplayStringAt(0, 150, "Bandpass", CENTER_MODE);
  int f_cutoff1 = 3400;
  int f_cutoff2 = 300;
  
  // lowpass coefficients
  double ff = f_cutoff1/sr;
  double coef = 1.0/tan(PI*ff);
  double sqrt2 = 1.41421356237;
  double b0, b1, b2, a1, a2;
  b0 = 1.0 / (1.0 + sqrt2*coef + coef*coef);
  b1 = 2*b0;
  b2 = b0;
  a1 = 2.0 * (coef*coef - 1.0) * b0;
  a2 = -(1.0 - sqrt2*coef + coef*coef) * b0;
  
  // highpass coefficients
  ff = f_cutoff2/sr;
  coef = 1.0/tan(PI*ff);
  double d0, d1, d2, c1, c2;
  d0 = 1.0 / (1.0 + sqrt2*coef + coef*coef);
  d1 = 2*d0;
  d2 = d0;
  c1 = 2.0 * (coef*coef - 1.0) * d0;
  c2 = -(1.0 - sqrt2*coef + coef*coef) * d0;
  d0 = d0*coef*coef;
  d1 = -d1*coef*coef;
  d2 = d2*coef*coef;
  
  // general
  int x0, x1, x2;
  int y1 = 0; int y2 = 0; int z1 = 0; int z2 = 0;
  int prevx1 = 0; int prevx2 = 0;
  int i = -1;
  
  while(cur_effect == 6)
  {
    if(i != i_sample)
    {
      // low pass filtering
      int temp, temp2;
      i = i_sample;
      if(i == 0)
      {
        x1 = prevx1;
        x2 = prevx2;
      }
      else if(i == 1)
      {
        x1 = *(INPUT + i - 1);
        x2 = prevx1;
      }
      else
      {
        x1 = *(INPUT + i - 1);
        x2 = *(INPUT + i - 2);
      }
      if (i == 1023)
      {
        prevx1 = *(INPUT + i);
        prevx2 = *(INPUT + i - 1);
      }
      x0 = *(INPUT + i);
      temp = (int)(b0*x0 + b1*x1 + b2*x2 + a1*y1 + a2*y2);
      
      // high pass filtering
      temp2 = (int)(d0*temp + d1*y1 + d2*y2 + c1*z1 + c2*z2);
      y2 = y1;
      y1 = temp;
      z2 = z1;
      z1 = temp2;
      *(OUTPUT + i) = temp2;
    }
    if(parameter_flag == 1)
    {
      parameter_flag = 0;
    }
  }
  BSP_LCD_DisplayStringAt(0, 150, "         ", CENTER_MODE);
}

//my voice: fundamental frequency 230 Hz +- 100
void PitchShift_effect(void)
{
  BSP_LCD_DisplayStringAt(0, 130, "Pitch", CENTER_MODE);
  BSP_LCD_DisplayStringAt(0, 170, "shift", CENTER_MODE);
  
  int bandpassed[1024];
  int oldx, period, len;
  int oldzero = 0;
  int i = 0;
  while(cur_effect == 7)
  {
    if(sample_finish_flag == 1)
    {
      sample_finish_flag = 0;
      // bandpass filtering
    {
    int f_cutoff1 = 3400;
    int f_cutoff2 = 300;
  
    double ff = f_cutoff1/sr;
    double coef = 1.0/tan(PI*ff);
    double sqrt2 = 1.41421356237;
    double b0, b1, b2, a1, a2;
    b0 = 1.0 / (1.0 + sqrt2*coef + coef*coef);
    b1 = 2*b0;
    b2 = b0;
    a1 = 2.0 * (coef*coef - 1.0) * b0;
    a2 = -(1.0 - sqrt2*coef + coef*coef) * b0;
  
    ff = f_cutoff2/sr;
    coef = 1.0/tan(PI*ff);
    double d0, d1, d2, c1, c2;
    d0 = 1.0 / (1.0 + sqrt2*coef + coef*coef);
    d1 = 2*d0;
    d2 = d0;
    c1 = 2.0 * (coef*coef - 1.0) * d0;
    c2 = -(1.0 - sqrt2*coef + coef*coef) * d0;
    d0 = d0*coef*coef;
    d1 = -d1*coef*coef;
    d2 = d2*coef*coef;
  
    int x0, x1, x2;
    int y1 = 0; int y2 = 0; int z1 = 0; int z2 = 0;
    int prevx1 = 0; int prevx2 = 0;
    for(int i = 0; i < 1023; i++)
      {
        int temp, temp2;
        if(i == 0)
        {
          x1 = prevx1;
          x2 = prevx2;
        }
        else if(i == 1)
        {
          x1 = *(DATA + i - 1);
          x2 = prevx1;
        }
        else
        {
          x1 = *(DATA + i - 1);
          x2 = *(DATA + i - 2);
        }
        if (i == 1023)
        {
          prevx1 = *(DATA + i);
          prevx2 = *(DATA + i - 1);
        }
        x0 = *(DATA + i);
        temp = (int)(b0*x0 + b1*x1 + b2*x2 + a1*y1 + a2*y2);
      
        temp2 = (int)(d0*temp + d1*y1 + d2*y2 + c1*z1 + c2*z2);
        y2 = y1;
        y1 = temp;
        z2 = z1;
        z1 = temp2;
        bandpassed[i] = temp2;
      }
    }
      int x0 = bandpassed[0];
      while(i < 1023)
      {
        i += 1;
        oldx = x0;
        x0 = bandpassed[i];
        if(oldx > 0 && x0 <= 0)
        {
          period = i - oldzero;
          oldzero = i;
        }
      }
    if(i == 1023)
    {
      len = 1024 - oldzero;
      for(int j = 0; j< len; j++) queue[j] = *(DATA + oldzero + j);
    }
    }
    if(parameter_flag == 1)
    {
      parameter_flag = 0;
    }
  }
  BSP_LCD_DisplayStringAt(0, 130, "         ", CENTER_MODE);
  BSP_LCD_DisplayStringAt(0, 170, "         ", CENTER_MODE);
}

void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 84;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
  
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC;
  PeriphClkInitStruct.PLLSAI.PLLSAIN = 60;
  PeriphClkInitStruct.PLLSAI.PLLSAIR = 5;
  PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_4;
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct);
  
  __HAL_RCC_SYSCFG_CLK_ENABLE();
}

static void MX_ADC1_Init(void)
{
  __HAL_RCC_ADC1_CLK_ENABLE();
  ADC_ChannelConfTypeDef sConfig = {0};

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T1_CC1;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  HAL_ADC_Init(&hadc1);
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_13;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);
  
  HAL_NVIC_EnableIRQ(ADC_IRQn);
}

static void MX_DAC_Init(void)
{
  __HAL_RCC_DAC_CLK_ENABLE();
  DAC_ChannelConfTypeDef sConfig = {0};

  hdac.Instance = DAC;
  HAL_DAC_Init(&hdac);

  sConfig.DAC_Trigger = DAC_TRIGGER_SOFTWARE;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_2);
}

static void MX_TIM1_Init(void)
{
  __HAL_RCC_TIM1_CLK_ENABLE();
  
  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};
  
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 9;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 380;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  HAL_TIM_Base_Init(&htim1);
  
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig);
  
  HAL_TIM_PWM_Init(&htim1);
  
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 380;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1);
  
  __HAL_TIM_DISABLE_OCxPRELOAD(&htim1, TIM_CHANNEL_1);
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig);
}

static void MX_GPIO_Init(void)
{
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  // DAC - OUT pin A5
  GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  //ADC1 - IN pin C3
  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
  //GPIO pin : PA0
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
}

void ADC_IRQHandler(void)
{
  HAL_ADC_IRQHandler(&hadc1);
  int out = (*(OUTPUT + i_sample) + 2048)*volume/100;
  HAL_DAC_SetValue(&hdac, DAC_CHANNEL_2, DAC_ALIGN_12B_R, out);
  HAL_DAC_Start(&hdac, DAC_CHANNEL_2);
  
  if (work_mode == 0) amplitude = HAL_ADC_GetValue(&hadc1) - 2048;
  else if (work_mode == 1) amplitude = (int)GetTestSound() - 2048;
  
  i_sample += 1;
  if (i_sample >= N_samples)
  {
    i_sample = 0;
    sample_finish_flag = 1;
    // changing input and data arrays
    int* temp = INPUT;
    INPUT = DATA;
    DATA = temp;
    //changing output and next_output arrays
    temp = OUTPUT;
    OUTPUT = NEXT_OUTPUT;
    NEXT_OUTPUT = temp;
  }
  *(INPUT + i_sample) = amplitude;
}

void EXTI0_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
  if (work_mode == 0)
  {
    work_mode = 1;
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    BSP_LCD_FillRect(0, 0, 80, 80);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_FillCircle(10, 35, 3);
    BSP_LCD_FillCircle(25, 30, 3);
    BSP_LCD_DrawLine(13, 35, 13, 15);
    BSP_LCD_DrawLine(13, 15, 28, 10);
    BSP_LCD_DrawLine(28, 10, 28, 30);
  }
  else if (work_mode == 1)
  {
    work_mode = 0;
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    BSP_LCD_FillRect(0, 0, 80, 80);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_DrawEllipse(18, 20, 7, 10);
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    BSP_LCD_FillRect(0, 0, 30, 25);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_FillEllipse(18, 17, 5, 9);
    BSP_LCD_DrawLine(13, 35, 23, 35);
    BSP_LCD_DrawLine(18, 30, 18, 35);
  }
}

void EXTI15_10_IRQHandler(void)
{
  EXTI->PR |= EXTI_IMR_MR15;
  BSP_TS_ITClear();
  TS_StateTypeDef TSState;
  BSP_TS_GetState(&TSState);
  int xt = (int)TSState.X;
  int yt = (int)TSState.Y;
  int t = HAL_GetTick();
  if(t - time > 50)
  {
    time = t;
    if(xt < 50 && yt > 100 && yt < 220)
  {
    if(cur_effect == 0) cur_effect = 7;
    else cur_effect -= 1;
  }
  else if(xt > 190 && yt > 100 && yt < 220)
  {
    if (cur_effect == 7) cur_effect = 0;
    else cur_effect += 1;
  }
  else if(yt > 280 && yt < 300 && xt < 220 && xt > 20)
  {
    volume = (int)((xt - 30)/1.8);
    if (volume < 0) volume = 0;
    if (volume > 100) volume = 100;
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    BSP_LCD_FillRect(24, 285, 193, 11);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_FillRect(30, 290, 180, 2);
    int pos = (int)(volume*1.8 + 30);
    BSP_LCD_FillCircle(pos, 290, 5);
    BSP_LCD_SetFont(&Font16);
    BSP_LCD_DisplayStringAt(0, 300, "           ", CENTER_MODE);
    uint8_t str[12];
    sprintf((char*)str, "Volume: %d", volume);
    BSP_LCD_DisplayStringAt(0, 300, str, CENTER_MODE);
    BSP_LCD_SetFont(&Font24);
  }
  else if(xt < 230 && xt > 20 && yt > 220 && yt < 280)
  {
    parameter_flag = 1;
    x = xt;
    y = yt;
  }
  }
 }